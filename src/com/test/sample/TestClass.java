package com.test.sample;

import java.util.Scanner;

import com.test.factory.PrintFacatory;
import com.test.factory.PrintProcessor;

public class TestClass {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Scanner input2 = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int n = input.nextInt();
		System.out.println("Enter a letter: ");
		String letter = input2.nextLine();
		input.close();
		input2.close();
		PrintProcessor printFacatory = PrintFacatory.getPrinter(letter);
		printFacatory.print(n);
	}

}
